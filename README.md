## By Mark Webley

### Charting Dashboard - with data coming in from node.js services first draft just data
![Alt text](screenshots/charts.png "Charting Dashboard - with data coming in from services first draft just data")



In the project directory, you can run:

### `npm install`
cd to main dir and run, this command, and also run this command inside,
the folder called node-server.

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run start`

**Note: CD to node-server directory and run npm run start**
