/**
 * Created by Mark Webley on 06/06/2019.
 */
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));

function randomisePositions() {
    let positions = [];
    for (let i = 0; i < 9; i++) {
        positions.push(Math.floor(Math.random() * 22));
    }
    return positions;
}

function chartRandomData() {
    return {
        charts: [
            {
                id: 1,
                layout: {
                    title: '<b>Scatter Chart</b><br>Mark Webley',
                    autosize: true,
                    xaxis: {
                        title: 'Time',
                        zeroline:false,
                    },
                    yaxis:{
                        title: 'Annotation',
                        zeroline:false
                    }
                },
                config: {
                    showLink: false,
                    displayModeBar: true
                },
                data: [
                    {
                        x: randomisePositions(),
                        y: randomisePositions(),
                        type: 'scatter',
                        name: 'Red',
                        mode: 'markers',
                        marker: {
                            size: 10,
                            color: 'red'
                        }
                    },
                    {
                        x: randomisePositions(),
                        y: randomisePositions(),
                        type: 'scatter',
                        name: 'blue',
                        mode: 'markers',
                        marker: {
                            size: 10,
                            color: 'blue'
                        }
                    },
                    {
                        x: randomisePositions(),
                        y: randomisePositions(),
                        type: 'scatter',
                        name: 'Orange',
                        mode: 'markers',
                        marker: {
                            size: 10,
                            color: 'orange'
                        }
                    }
                ]
            },
            {
                id: 2,
                layout: {
                    title: '<b>Scatter Chart</b><br>Mark Webley',
                    autosize: true,
                    xaxis: {
                        title: 'Time',
                        zeroline:false,
                    },
                    yaxis:{
                        title: 'Annotation',
                        zeroline:false
                    }
                },
                config: {
                    showLink: false,
                    displayModeBar: true
                },
                data: [
                    {
                        x: randomisePositions(),
                        y: randomisePositions(),
                        type: 'scatter',
                        name: 'Red',
                        mode: 'markers',
                        marker: {
                            size: 10,
                            color: 'red'
                        }
                    },
                    {
                        x: randomisePositions(),
                        y: randomisePositions(),
                        type: 'scatter',
                        name: 'blue',
                        mode: 'markers',
                        marker: {
                            size: 10,
                            color: 'blue'
                        }
                    },
                    {
                        x: randomisePositions(),
                        y: randomisePositions(),
                        type: 'scatter',
                        name: 'Orange',
                        mode: 'markers',
                        marker: {
                            size: 10,
                            color: 'orange'
                        }
                    }
                ]
            },
            {
                id: 3,
                layout: {
                    title: '<b>Scatter Chart</b><br>Mark Webley',
                    autosize: true,
                    xaxis: {
                        title: 'Time',
                        zeroline:false,
                    },
                    yaxis:{
                        title: 'Annotation',
                        zeroline:false
                    }
                },
                config: {
                    showLink: false,
                    displayModeBar: true
                },
                data: [
                    {
                        x: randomisePositions(),
                        y: randomisePositions(),
                        type: 'scatter',
                        name: 'Red',
                        mode: 'markers',
                        marker: {
                            size: 10,
                            color: 'red'
                        }
                    },
                    {
                        x: randomisePositions(),
                        y: randomisePositions(),
                        type: 'scatter',
                        name: 'blue',
                        mode: 'markers',
                        marker: {
                            size: 10,
                            color: 'blue'
                        }
                    },
                    {
                        x: randomisePositions(),
                        y: randomisePositions(),
                        type: 'scatter',
                        name: 'Orange',
                        mode: 'markers',
                        marker: {
                            size: 10,
                            color: 'orange'
                        }
                    }
                ]
            },
            {
                id: 4,
                layout: {
                    title: '<b>Scatter Chart</b><br>Mark Webley',
                    autosize: true,
                    xaxis: {
                        title: 'Time',
                        zeroline:false,
                    },
                    yaxis:{
                        title: 'Annotation',
                        zeroline:false
                    }
                },
                config: {
                    showLink: false,
                    displayModeBar: true
                },
                data: [
                    {
                        x: randomisePositions(),
                        y: randomisePositions(),
                        type: 'scatter',
                        name: 'Red',
                        mode: 'markers',
                        marker: {
                            size: 10,
                            color: 'red'
                        }
                    },
                    {
                        x: randomisePositions(),
                        y: randomisePositions(),
                        type: 'scatter',
                        name: 'blue',
                        mode: 'markers',
                        marker: {
                            size: 10,
                            color: 'blue'
                        }
                    },
                    {
                        x: randomisePositions(),
                        y: randomisePositions(),
                        type: 'scatter',
                        name: 'Orange',
                        mode: 'markers',
                        marker: {
                            size: 10,
                            color: 'orange'
                        }
                    }
                ]
            }
        ]
    }
}


app.get('/api/charting', (req, res) => {
    const name = req.query.name || 'World';
    res.setHeader('Content-Type', 'application/json');
    let data = chartRandomData();
    res.send(JSON.stringify({
        data
    }));
});

app.listen(3001, () =>
    console.log('Express server is running on localhost:3001')
);