/**
 * Created by Mark Webley on 07/06/2019.
 */
import React, {PureComponent} from 'react';
import createPlotlyComponent from 'react-plotlyjs';
import Plotly from 'plotly.js-dist';
import regression from 'regression';
import './style.scss';

const PlotlyComponent = createPlotlyComponent(Plotly);

export class ScatterGraphComponent extends PureComponent {

    regressionLines = 0;

    constructor(props) {
        super(props);
    }

    handleChange = (event) => {
        this.setState({name: event.target.value});
    }

    setLinearRegression(data) {
        let regressionPairs = [];
        for(let i = 0; i < data.x.length; i++) {
            regressionPairs.push([data.x[i],data.y[i]]);
        }

        // regression.linear
        // regression.polynomial
        let regressionTest = regression.linear(regressionPairs);
        let regressionResultX = regressionTest.points.map(p => p[0]);
        let regressionResultY = regressionTest.points.map(p => p[1]);

        return {
            x: regressionResultX,
            y: regressionResultY,
            name: data.name,
            type: data.type,
            marker: data.marker
        };
    }

    setCharts() {
        let data = [];
        let charts = this.props.data;
        let traceLinearRegression;
        charts.forEach((chart) => {
            traceLinearRegression = this.setLinearRegression(chart);
            data.push(chart);
            data.push(traceLinearRegression);
            this.regressionLines++;
        });
        return data;
    }

    toggleLinearRegression = (event) => {
        event.preventDefault();
        event.stopPropagation();
    }

    onClick = (event) => {

    }

    onSelected = (event) => {

    }

    render() {
        const data = this.setCharts();
        let classAssignment = '';
        const buttons = this.props.data.map(data => {
            classAssignment = `btn btn-primary ${data.marker.color}`;
            return (
                <button key={data.marker.color} className={classAssignment} >toggle linear regression</button>
            );
        });

        return (
            <>
                <PlotlyComponent
                    className="whatever"
                    onClick={this.onClick}
                    onSelected={this.onSelected}
                    data={data}
                    layout={this.props.layout}
                    config={this.props.config}
                />
                <div  onClick={this.toggleLinearRegression}>
                    {buttons}
                </div>
            </>
        );
    }
}