import React, {Component} from 'react';
import {ScatterGraphComponent} from "../../common/charts/scatter/ScatterGraphComponent";
import {Loader} from '../../common/loader/loader.component';

export default class MainContent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            charts: []
        };
    }

    getData() {
       fetch(`/api/charting?name=${encodeURIComponent(this.state.name)}`)
            .then(response => response.json())
            .then(state => {
            this.setState({
                charts: state.data.charts
            });
        });
    }

    componentDidMount() {
        this.getData();
    }
    toggleLinearRegression = (event) => {
        event.preventDefault();
        event.stopPropagation();
    }

    render() {
        if (typeof this.state.charts === "undefned" || this.state.charts.length === 0) {
            return (
                <Loader />
            )
        }
        const firstRow = this.state.charts.slice(0,2);
        const secondRow = this.state.charts.slice(2,4);
        return (
            <>
                <div className="row">
                    {
                        firstRow.map((chart, i) => {
                            return (
                                <div key={chart.id} className="card mr-2 mb-2 col">
                                    <div className="card-body">
                                        <ScatterGraphComponent {...chart} />
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
                <div className="row">
                    {
                        secondRow.map((chart, i) => {
                            return (
                                <div key={chart.id} className="card mr-2 mb-2 col">
                                    <div className="card-body">
                                        <ScatterGraphComponent {...chart} />
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </>
        );
    }
}
