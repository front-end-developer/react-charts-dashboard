/**
 * Created by Mark Webley on 08/06/2019.
 */
import React, {Component} from 'react';
import './style.scss';

export default class TopContent extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <nav className="navbar navbar-custom">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#">
                            <img alt="Brand" src="images/my-logo.gif" />
                        </a>
                    </div>
                </div>
            </nav>
        );
    }
}