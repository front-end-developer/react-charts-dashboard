/**
 * Created by Mark Webley on 08/06/2019.
 */
import React, {Component} from 'react';
import './style.scss';

export default class LeftContent extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <nav className="navbar-left"></nav>
        );
    }
}