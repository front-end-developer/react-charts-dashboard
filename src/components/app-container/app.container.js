import React, {Component} from 'react';
import MainContent from "./main-content/main-content";
import TopContent from "./top-content/top-content";
import LeftContent from "./left-content/left-content";
import './style.scss';

export default class AppContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>
                <TopContent/>
                <LeftContent/>
                <section className="App container-fluid">
                    <div className="custom-position col-12">
                        <MainContent />
                    </div>
                </section>
            </>
        );
    }
}