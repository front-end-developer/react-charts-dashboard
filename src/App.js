import React, {Component} from 'react';
import './App.scss';

import AppContainer from './components/app-container/app.container';

function App() {
    return (
        <div className="App">
            <AppContainer />
        </div>
    );
}

export default App;

